﻿using UnityEngine;
using System.Collections;

public class HealthBarScript : MonoBehaviour {

    public SpriteRenderer LL, LM, MM, MR, RR;
    public Sprite le, lf, me, mf, re, rf;

    void Update()
    {

        UpdateHP(FindObjectOfType<MainCharacter>().health);

    }

	// Use this for initialization
	public void UpdateHP(float currentHP)
    {

        print(currentHP);

        if(currentHP == 50)
        {

            RR.sprite = rf;
            MR.sprite = mf;
            MM.sprite = mf;
            LM.sprite = mf;
            LL.sprite = lf;


        }

        else if(currentHP >= 40)
        {

            RR.sprite = re;
            MR.sprite = mf;
            MM.sprite = mf;
            LM.sprite = mf;
            LL.sprite = lf;

        }

        else if(currentHP >= 30)
        {

            RR.sprite = re;
            MR.sprite = me;
            MM.sprite = mf;
            LM.sprite = mf;
            LL.sprite = lf;

        }

        else if(currentHP >= 20)
        {

            RR.sprite = re;
            MR.sprite = me;
            MM.sprite = me;
            LM.sprite = mf;
            LL.sprite = lf;

        }

        else if(currentHP >= 10)
        {

            RR.sprite = re;
            MR.sprite = me;
            MM.sprite = me;
            LM.sprite = me;
            LL.sprite = lf;

        }

        else if(currentHP <= 0)
        {

            RR.sprite = re;
            MR.sprite = me;
            MM.sprite = me;
            LM.sprite = me;
            LL.sprite = le;

        }

    }

}
