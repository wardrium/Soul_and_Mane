﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Platform : MonoBehaviour {
    public float z_rotation = 0.0f;
    public int width = 1;
    public int height = 1;

    public GameObject bricks_prefab;

    public List<GameObject> cubes = new List<GameObject>();
    public BoxCollider bc;

    public bool toUpdate = true;

    void construct(int width, int height) {
        Vector3 orig_pos = this.transform.position;
        Vector3 xpos, ypos;

        // Generate textured cubes
        for (int x = 0; x < width; x++) {
            xpos = orig_pos + x * new Vector3(1.0f, 0f, 0f);

            for (int y = 0; y < height; y++) {
                // Create cubes
                ypos = xpos + y * new Vector3(0f, 1.0f, 0f);

                GameObject go = Instantiate(bricks_prefab);
                go.transform.parent = this.transform;
                go.transform.position = ypos;
                //go.transform.localScale = 
            }
        }

        // Update bounding box
        if (bc != null) {
            bc.size = new Vector3( (float) width, (float) height, 1.0f);
            bc.center = this.transform.position + new Vector3((float) (width - 1) / 2, (float) (height-1)/2, 0f);
        }
    }

    void updateCubes() {
        //Destroy all previous cubes
        foreach (GameObject go in cubes) {
            Destroy(go);
        }

        construct(this.width, this.height);
        if (this.z_rotation != 0.0f) {
            this.transform.Rotate(new Vector3(0.0f, 0.0f, z_rotation));
        }
    }

	// Use this for initialization
	void Start () {
        bc = this.gameObject.GetComponent<BoxCollider>();

        if(bc == null) {
            bc = this.gameObject.AddComponent<BoxCollider>();
        }
	}

    void Update() {
        if (toUpdate) {
            updateCubes();
            toUpdate = false;
        }
    }
}
