﻿using UnityEngine;
using System.Collections;

public class Swipe : MonoBehaviour {

    private float birth;
    public float lifeTime = .1f;
    public float repulse = 3f;

    // Use this for initialization
    void Start() {
        birth = Time.time;
    }

    // Update is called once per frame
    void Update() {
        if (Time.time > birth + lifeTime) {
            Destroy(gameObject);
        }

        
    }

    void LateUpdate() {
        transform.position = MainCharacter.S.swipeLoc.position;
    }

    void OnTriggerEnter(Collider coll) {
        if (coll.CompareTag("Elephant") || coll.CompareTag("Narwhal") || coll.CompareTag("Hopper")) {
            Vector3 direction = (coll.transform.position - transform.position).normalized;
            if (direction.y < 0)
            {
                direction.y = 0;
            }

            Enemy e = coll.GetComponent<Enemy>();

            e.health -= 5;
            if (e.health <= 0) {
                e.Die();
            }

            if (e != null)
            {
                e.SwipedFunction(transform.position);
                e.swipeTime = .1f;
            }
            //coll.transform.position += direction * repulse;

            if (coll.CompareTag("Narwhal")) {
                Narwhal n = coll.gameObject.GetComponent<Narwhal>();
                n.Swiped();
            }
        }
    }
}
