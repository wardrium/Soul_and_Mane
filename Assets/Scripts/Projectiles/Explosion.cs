﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

    private AudioSource boom;
    private float birthTime;

	// Use this for initialization
	void Start () {
        boom = GetComponent<AudioSource>();
        boom.Play();
        birthTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
	    if (Time.time > birthTime + 0.5f) {
            Destroy(gameObject);
        }
	}
}
