﻿using UnityEngine;
using System.Collections;

public class Bolt : MonoBehaviour {

    private float beginLife;
    private float lifeTime = 1f;

	// Use this for initialization
	void Start () {
        beginLife = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        float u = (Time.time - beginLife) / lifeTime;
	    if (u > 1) {
            Destroy(gameObject);
        }

        Vector3 scale = transform.localScale;
        scale.x = scale.z = 0.5f * (1 - u);
        transform.localScale = scale;
	}
}
