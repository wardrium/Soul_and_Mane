﻿using UnityEngine;
using System.Collections;

public class Missile : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if ((transform.position - King.S.transform.position).magnitude > 50
            && (transform.position - MainCharacter.S.transform.position).magnitude > 50) {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider coll) {
        if (coll.CompareTag("Solid")) {
            Destroy(gameObject);
        }

        if(coll.tag != "Player")
        {

            Destroy(gameObject);

        }
    }
}
