﻿using UnityEngine;
using System.Collections;

public class HarambeBall : MonoBehaviour {

    void OnTriggerEnter(Collider coll) {
        if (coll.gameObject.layer == LayerMask.NameToLayer("Environment")) {
            Destroy(this.gameObject);
        }
        else if (coll.gameObject.layer == LayerMask.NameToLayer("Players")) {
            Destroy(this.gameObject);
        }
    }
}
