﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {
    public GameObject explosionParticles;
    public Sprite[] sprites;
    private int spriteIndex;
    private SpriteRenderer sprend;

    private float update_animation_cooldown = 0.5f; // How long to wait before updating animation.

    private float time_since_last_animation_change = 0.0f;

	void Start () {
        spriteIndex = 0;
        sprend = this.GetComponent<SpriteRenderer>();
	}
	
	void Update () {
        time_since_last_animation_change += Time.deltaTime;
        if (time_since_last_animation_change >= update_animation_cooldown) {
            time_since_last_animation_change = 0.0f;
            spriteIndex++;
            if (spriteIndex == sprites.Length) {
                Explode();
            }
            else {
                sprend.sprite = sprites[spriteIndex];
            }
        }
	}

    private void Explode() {
        GameObject.Instantiate(explosionParticles, this.transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
