﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
    public ParticleSystem ps;
    public float particleRadius = 2;

    void Start() {
        ps = this.GetComponentInChildren<ParticleSystem>();
        if (ps != null) {
            var em = ps.emission;
            em.enabled = false;
        }
    }

    void FixedUpdate() {
        if (ps == null) { return; }
        Vector3 dist1 = this.transform.position - MainCharacter.S.transform.position;
        Vector3 dist2 = this.transform.position - King.S.transform.position;

        if (dist1.magnitude < particleRadius || dist2.magnitude < particleRadius) {
            var em = ps.emission;
            em.enabled = true;
        } else {
            var em = ps.emission;
            em.enabled = false;
        }
    }
}
