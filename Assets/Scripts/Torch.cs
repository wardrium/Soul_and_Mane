﻿using UnityEngine;
using System.Collections;

public class Torch : MonoBehaviour {

    public bool on = false;
    GameObject orb;
    GameObject particles;

    void Start() {
        Transform t = this.gameObject.transform.Find("Sphere");
        if (t != null) {
            orb = t.gameObject;
            orb.SetActive(false);
        }

        t = this.gameObject.transform.Find("Particles");
        if (t != null) {
            particles = t.gameObject;
            particles.SetActive(false);
        }

        turnOff();
    }


    void toggle(bool b) {
        on = b;
        if (orb != null) {
            orb.SetActive(b);
        }

        if (particles != null) {
            particles.SetActive(b);
        }
    }


    public void turnOn() {
        toggle(true);
    }

    public void turnOff() {
        toggle(false);
    }

    public bool isOn() {
        return on;
    }
}