﻿using UnityEngine;
using System.Collections;
using InControl;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    InputDevice device;

	// Use this for initialization
	void Start () {
        device = InputManager.ActiveDevice;
	}
	
	// Update is called once per frame
	void Update () {
        device = InputManager.ActiveDevice;
        if (device.MenuWasPressed) {
            SceneManager.LoadScene("_Scene_0");
        }
	}
}
