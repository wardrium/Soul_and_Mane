﻿using UnityEngine;
using System.Collections;

public class OpenDoorBoltTrigger : BoltTrigger {

    public GameObject door;

    public override void Activate() {
        base.Activate();
        Destroy(door);
    }
}
