﻿using UnityEngine;
using System.Collections;

public enum Triggertype
{

    LION_STAND,
    KING_BOLT

}

public class Trigger : MonoBehaviour {

    public bool isActivated = false;

    public Triggertype tt;

    void OnTriggerEnter(Collider other)
    {

        if((other.name == "Bullet(Clone)" || other.name == "Bolt(Clone)") && tt == Triggertype.KING_BOLT)
        {

            isActivated = true;

            Torch t = transform.Find("Torch").GetComponent<Torch>();
            t.turnOn();
        }

        if (FindObjectOfType<MainCharacter>().GetComponent<Collider>() == other && Triggertype.LION_STAND == tt)
        {

            isActivated = true;

        }

        print(other.name);

    }

    void OnTriggerExit(Collider other)
    {

        if(FindObjectOfType<MainCharacter>().GetComponent<Collider>() == other)
        {

            isActivated = false;

        }

    }

}
