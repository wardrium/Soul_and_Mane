﻿using UnityEngine;
using System.Collections;

public class DoorBehavior : MonoBehaviour {

    public Trigger t1, t2, t3, t4;
    public GameObject moveThisObject;
    bool openSesame = false;

    public Vector3 movedistance;

    public int numIterations;
    int currNumIterations;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if(t1.isActivated && (t2 == null || t2.isActivated) && (t3 == null || t3.isActivated) && (t4 == null || t4.isActivated))
        {

            openSesame = true;

        }

        if(openSesame)
        {

            if(currNumIterations < numIterations)
            {

                currNumIterations++;
                moveThisObject.transform.position += movedistance;

            }

        }

	}
    
}
