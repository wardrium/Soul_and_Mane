﻿using UnityEngine;
using System.Collections;

public class BoltTrigger : MonoBehaviour {

    private bool activated = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider coll) {
        if (coll.CompareTag("KingBolt")) {
            Activate();
        }
    }

    public virtual void Activate() {
        activated = true;

        GetComponent<Renderer>().material.color = Color.blue;
    }
}
