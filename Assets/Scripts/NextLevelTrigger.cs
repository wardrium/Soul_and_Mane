﻿using UnityEngine;
using System.Collections;

public class NextLevelTrigger : MonoBehaviour {

    public bool isKingTrigger = false;
    public bool canSwitchLevels = true;

	// Use this for initialization
	void OnTriggerEnter(Collider other)
    {

        canSwitchLevels = (FindObjectOfType<King>().GetComponent<Collider>() == other && isKingTrigger) || (FindObjectOfType<MainCharacter>().GetComponent<Collider>() == other && !isKingTrigger);

    }

    void OnTriggerExit(Collider other)
    {

        if(other.name == "King" && isKingTrigger)
        {

            canSwitchLevels = false;

        }

        else if(FindObjectOfType<MainCharacter>().GetComponent<Collider>() == other && !isKingTrigger)
        {

            canSwitchLevels = false;

        }

    }

}
