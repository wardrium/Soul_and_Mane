﻿using UnityEngine;
using System.Collections;

public class BGMusic : MonoBehaviour {

    public static BGMusic S;

	// Use this for initialization
	void Awake () {
        if (S != null) {
            Destroy(gameObject);
        } else {
            S = this;
        }

        DontDestroyOnLoad(transform.gameObject);
    }
	
}
