﻿using UnityEngine;
using System.Collections;

public class FadeIn : MonoBehaviour {

    public SpriteRenderer sprend;

    void Awake()
    {

        transform.position = new Vector3(0, 0, -2);
        sprend.color = new Color(sprend.color.r, sprend.color.g, sprend.color.b, 1);

    }

	// Update is called once per frame
	void Update () {

        Color c = new Color(sprend.color.r, sprend.color.g, sprend.color.b, sprend.color.a);

        c.a -= .01f;

        print(c.a);

        if(c.a <= 0)
        {

            Destroy(gameObject);

        }
        
        else
        {

            sprend.color = c;

        }

	}

}
