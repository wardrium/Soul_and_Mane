﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {

    public SpriteRenderer sprend;
    bool getDark = false;
    // Use this for initialization
    void Awake()
    {

        transform.position = new Vector3(0, 0, 4);

    }

    public void getDarker()
    {

        getDark = true;
        transform.position = new Vector3(0, 0, -2);

    }

    // Update is called once per frame
    void Update()
    {

        if (getDark)
        {
            Color c = new Color(sprend.color.r, sprend.color.g, sprend.color.b, sprend.color.a);

            c.a += .01f;

            print(c.a);

            if (c.a >= 1)
            {

            }

            else
            {

                sprend.color = c;

            }
        }

    }

}
