﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

    public string loadThisLevel = "_Scene_1";

    public SpriteRenderer sprend;
    bool loadLevel = false;

    int blinkieblinkie = 0;

    public GUIText gui;
    Color guiColor;

    int guiCount = 0;

	// Use this for initialization
	void Start () {

        guiColor = new Color(gui.color.r, gui.color.g, gui.color.b, gui.color.a);
	
	}
	
	// Update is called once per frame
	void Update () {
	
        if(Input.GetKeyDown(KeyCode.Joystick1Button7) ||
                Input.GetKeyDown(KeyCode.Joystick2Button7))
        {

            GetComponent<AudioSource>().Play();
            loadLevel = true;

        }

        if (loadLevel)
        {


            Color c = new Color(sprend.color.r, sprend.color.g, sprend.color.b, sprend.color.a);
            c.a -= .05f;

            if (c.a > 0)
            {

                sprend.color = c;
            }

            guiCount++;

            if (guiCount < 120)
            {
                if ((guiCount / 3) % 2 == 1)
                {

                    gui.color = new Color(0, 0, 0, 0);

                }

                else
                {

                    gui.color = guiColor;

                }

            }

            else
            {

                gui.color = new Color(0, 0, 0, 0);

            }

            if (guiCount >= 300)
            {

                Application.LoadLevel("_Scene_0");

            }

        }

        else
        {


            blinkieblinkie++;

            if((blinkieblinkie/30) % 2 == 1)
            {

                gui.color = new Color(0, 0, 0, 0);

            }

            else
            {

                gui.color = guiColor;

            }

        }

	}

}
