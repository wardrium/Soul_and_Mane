﻿using UnityEngine;
using System.Collections;

public class TextureTiling : MonoBehaviour {
    public Material mat;

	void Start () {
        this.GetComponent<Renderer>().material = mat;
        this.GetComponent<Renderer>().material.mainTextureScale = new Vector2(this.transform.localScale.x, this.transform.localScale.y);
	}
	
}
