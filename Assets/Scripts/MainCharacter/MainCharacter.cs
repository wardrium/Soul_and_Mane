﻿using UnityEngine;
using System.Collections;
using InControl;
using UnityEngine.SceneManagement;

public class MainCharacter : MonoBehaviour {

    InputDevice device;

    public static MainCharacter S;

    public float maxHealth = 10;
    public float health;

    public bool mercyInvincible = false;
    private float mercyInvincibleStart;
    public float mercyInvincibleTime = 3f;

    public float jumpHoldTime = 0;
    Rigidbody rigid;
    ParticleSystem particlesystem;

    public float animationCounter = 0;

    private SpriteRenderer[] sprend;
    private SpriteRenderer feet;
    public Sprite[] feets;
    public float spriteChangeTime = 0.2f;
    private AudioSource audioSource;

    public GameObject swipe;
    public Transform swipeLoc;
    private float lastSwipeTime;
    float swipeCooldown = .55f;

    int deathCount = 0;

    bool ____________________TOPDOWNVARIABLES___________________;

    bool canMoveRight, canMoveLeft, canMoveDown, canMoveUp;

    bool _______________MODIFIABLES__________________;
    public float CollidableDistance;
    public float MoveSpeed;

    bool _______________GRAVITYVARIABLES________________;
    float currentFallSpeed = 0, maxFallSpeed = .4f, fallSpeedConstant = .008f;
    float deathCounter = 0;
    public float currentSpeedR, currentSpeedL, maxSpeed, acceleration;
    public float jumpSpeed;

    // Use this for initialization
    void Start() {
        sprend = transform.Find("Sprites").GetComponentsInChildren<SpriteRenderer>();

        feet = transform.Find("Sprites").Find("Feet").GetComponent<SpriteRenderer>();

        swipeLoc = transform.Find("SwipeLoc");

        audioSource = GetComponent<AudioSource>();

        health = maxHealth;

        if (InputManager.Devices.Count < 2) {
            Debug.LogError("Only " + InputManager.Devices.Count + " devices plugged in.");
            this.enabled = false;
            return;
        }
        device = InputManager.Devices[1];

        S = this;

        rigid = GetComponent<Rigidbody>();
        currentFallSpeed = 0;

        currentSpeedR = 0;
        currentSpeedL = 0;
        maxSpeed = 10f;
        acceleration = 2f;

    }

    // Set the current controller
    public void Assign(InputDevice i) {
        device = i;
    }

    // Update is called once per frame
    void Update() {

        CollisionHandler();

        if(Input.GetKeyDown(KeyCode.F1))
        {

            health = maxHealth;
            print("I'm a little cheater");

        }

        MovementHandler2DPlatformer();
        if (health <= 0) {

            deathCounter += Time.deltaTime;
            deathCount++;

            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            FindObjectOfType<King>().GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;



            transform.rotation = Quaternion.Euler(0, 0, 180);
            FindObjectOfType<King>().transform.rotation = Quaternion.Euler(0, 0, 180);


            if(deathCounter < .75f)
            {

                transform.position = new Vector3(transform.position.x, transform.position.y + .1f, 0);
                FindObjectOfType<King>().transform.position = new Vector3(FindObjectOfType<King>().transform.position.x, FindObjectOfType<King>().transform.position.y + .1f, 0);


            }

            else
            {

                transform.position = new Vector3(transform.position.x, transform.position.y - .1f, 0);
                FindObjectOfType<King>().transform.position = new Vector3(FindObjectOfType<King>().transform.position.x, FindObjectOfType<King>().transform.position.y - .1f, 0);


            }

            if(deathCounter >= 5)
            {

                SceneChanger sc = FindObjectOfType<SceneChanger>();
                Application.LoadLevel(SceneManager.GetActiveScene().name);
                sc.goToNextLevel = true;

            }

            /*
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            */
        }

        if (device.Action2.WasPressed && Time.time > lastSwipeTime + swipeCooldown) {
            lastSwipeTime = Time.time;
            GameObject go = Instantiate(swipe);
            audioSource.Play();
            go.transform.position = swipeLoc.position;
            go.transform.rotation = transform.rotation;
        }

	}

    void LateUpdate() {
        if (mercyInvincible) {
            for (int i = 0; i < sprend.Length; ++i) {
                sprend[i].enabled = !sprend[i].enabled;
            }
            if(Time.time > mercyInvincibleStart + mercyInvincibleTime) {
                mercyInvincible = false;
            }

        } else {
            for (int i = 0; i < sprend.Length; ++i) {
                sprend[i].enabled = true;
            }
        }

        if (currentSpeedL != 0 || currentSpeedR != 0) {
            float index = Time.time % spriteChangeTime;
            if (index < spriteChangeTime / 2) {
                feet.sprite = feets[0];
            } else {
                feet.sprite = feets[1];
            }
        } else {
            feet.sprite = feets[0];
        }
    }

    void MovementHandler2DPlatformer() {

        if (device == null) { return; }
        float horizontalAxis = device.LeftStickX.Value;
        Vector3 moveVector = new Vector3(0, 0, 0);

        /*Handles Jump*/

        bool jump = device.Action1.WasPressed;




        if (!canMoveDown && !jump)
        {

            moveVector.y = 0;

        }



        bool jumpRelease = !device.Action1.IsPressed;

        if(jump && !canMoveDown && canMoveUp)
        {

            moveVector.y = 16f;
            jump = false;

        }

        



        if(canMoveDown && rigid.velocity.y > -20f)
        {

            rigid.velocity = new Vector3(rigid.velocity.x, rigid.velocity.y - .2f, 0);

        }

        if(jumpRelease && -currentFallSpeed > 0)
        {

            if(currentFallSpeed >= 0)
            {

                currentFallSpeed /= 1.01f;

            }

            currentFallSpeed += .005f;

            jump = false;

        }

        if (horizontalAxis > 0)
        {
            transform.rotation = Quaternion.identity;



            if (!canMoveDown)
            {

                currentSpeedL = 0;
                currentSpeedR += .6f;

            }

            else
            {

                currentSpeedR += .1f;

            }

            if(currentSpeedR >= 15)
            {

                currentSpeedR = 15;

            }

            moveVector.x = currentSpeedR + currentSpeedL;

        }

        if (horizontalAxis < 0) {
            transform.rotation = Quaternion.Euler(0, 180, 0);

            if (!canMoveDown)
            {
                currentSpeedR = 0;
                currentSpeedL -= .6f;
            }
            else
            {

                currentSpeedL -= .1f;

            }
            if(currentSpeedL <= -15)
            {

                currentSpeedL = -15;

            }

            moveVector.x = currentSpeedL;

        }

        if(device.LeftStickX.Value == 0)//Input.GetAxis("Horizontal") == 0)
        {

            currentSpeedL = 0;
            currentSpeedR = 0;

        }

        //print(moveVector);

        if (jumpRelease && moveVector.y + rigid.velocity.y > 0)
        {

            moveVector.y -= .4f;

        }

        rigid.velocity = new Vector3(moveVector.x, moveVector.y + rigid.velocity.y, 0);

        if (rigid.velocity.y > 16)
        {

            rigid.velocity = new Vector3(moveVector.x, 10, 0);

        }

    }

    void CollisionHandler() {

        Vector3 z, c;

        z = new Vector3(transform.position.x - 1f, transform.position.y, transform.position.z);
        c = new Vector3(transform.position.x + 1f, transform.position.y, transform.position.z);

        canMoveUp = !Physics.Raycast(transform.position, new Vector3(0, 1, 0), 1.5f, LayerMask.GetMask("Environment")) &&
            !Physics.Raycast(z, new Vector3(0, 1, 0), 1.5f, LayerMask.GetMask("Environment")) &&
            !Physics.Raycast(c, new Vector3(0, 1, 0), 1.5f, LayerMask.GetMask("Environment"));

        canMoveDown = !Physics.Raycast(transform.position, new Vector3(0, -1, 0), 1.5f, LayerMask.GetMask("Environment")) &&
            !Physics.Raycast(z, new Vector3(0, -1, 0), 1.5f, LayerMask.GetMask("Environment")) &&
            !Physics.Raycast(c, new Vector3(0, -1, 0), 1.5f, LayerMask.GetMask("Environment"));


    }

    void OnTriggerEnter(Collider coll) {
        if (mercyInvincible) {
            return;
        }

        if (coll.gameObject.layer == LayerMask.NameToLayer("Enemy")) {
            HitEnemy(coll);
        }
    }

    public void HitEnemy(Collider coll) {
        mercyInvincible = true;
        mercyInvincibleStart = Time.time;
        switch (coll.tag) {
            case "Narwhal":
                health -= 5;
                break;
            case "Hopper":
                health -= 8;
                break;
            case "Elephant":
                health -= 5;
                break;
            case "Explosion":
                health -= 10;
                break;
            case "Worm":
                health -= 5;
                break;
            case "HarambeBall":
                health -= 5;
                break;
            default:
                break;
        }
    }
}