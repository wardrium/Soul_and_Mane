﻿using UnityEngine;
using System.Collections;
using InControl;
using UnityEngine.SceneManagement;

public class King : MonoBehaviour {

    InputDevice device;

    // Singleton variable
    public static King S;

    public GameObject missile;
    public GameObject bolt;

    // Sprites
    private SpriteRenderer sprend;
    public Sprite neutral, left, right;
    public Sprite left2, right2;

    private Transform eyes;
    public Sprite[] eyeList;

    private SpriteRenderer prompt;

    // Sounds
    private AudioSource jumpSound, lightningSound, missileSound;

    // Movement variables
    private Rigidbody rigid;

    public bool mounted;
    public float maxHorizSpeed;
    private float currentHorizSpeed;

    public float jumpSpeed;
    public bool grounded = true;
    public bool leftBounded;
    public bool rightBounded;

    public float gravity;
    public bool usingGravity;
    private int groundPhysicsLayerMask;

    public float maxMountDist;

    // Weapon variables
    public float angle;
    public float timeBetweenShots_Mounted;
    public float timeBetweenShots_Dismounted;
    private float lastShotTime;

    public float timeBetweenLightning;
    public float lightningChargeTime;
    private float lastLightningTime;
    private bool lightningCharging = false;
    private float lightningChargeStart;

    private Transform weapon;

	// Use this for initialization
	void Start () {
        if (InputManager.Devices.Count < 2) {
            Debug.LogError("Only " + InputManager.Devices.Count + " devices plugged in.");
            this.enabled = false;
            return;
        }
        device = InputManager.Devices[0];

        S = this;

        sprend = transform.Find("Sprite").GetComponent<SpriteRenderer>();
        eyes = transform.Find("Eyes");

        prompt = transform.Find("MountUpButton").GetComponent<SpriteRenderer>();

        rigid = GetComponent<Rigidbody>();
        groundPhysicsLayerMask = LayerMask.GetMask("Environment");
        weapon = transform.Find("Weapon");

        GetComponent<Renderer>().material.color = Color.white;

        jumpSound = transform.Find("JumpSound").GetComponent<AudioSource>();
        lightningSound = transform.Find("LightningSound").GetComponent<AudioSource>();
        missileSound = transform.Find("MissileSound").GetComponent<AudioSource>();
    }
	
    // Set the current controller
    public void Assign(InputDevice i) {
        device = i;
    }

	// Update is called once per frame
	void Update () {
        CheckDirection();
        FireControl();
        GetMovement();
        ChangeMount();
	}

    // handle movement
    void FixedUpdate () {
        if (!mounted) {
            Move();
        }
    }

    // LateUpdate executes after every update statement
    void LateUpdate () {
        // Jump to the lion's position if mounted
        if (mounted) {
            UpdatePosition();
        }

        // Pick the correct player sprite
        float index = Time.time % 0.4f;
        int i = 1;
        if (index < 0.2) {
            i = 0;
        }

        if (currentHorizSpeed > 0) {
            if (i == 1) {
                sprend.sprite = right;
            } else {
                sprend.sprite = right2;
            }
        } else if (currentHorizSpeed < 0) {
            if (i == 1) {
                sprend.sprite = left;
            } else {
                sprend.sprite = left2;
            }
        } else {
            sprend.sprite = neutral;
        }

        // Pick the correct eye sprite and location
        if (currentHorizSpeed > 0) {
            eyes.GetComponent<SpriteRenderer>().sprite = eyeList[1];
            eyes.GetComponent<SpriteRenderer>().flipY = false;
            eyes.localPosition = new Vector3(0.2f, 0.25f, 0);
        } else if (currentHorizSpeed < 0) {
            eyes.GetComponent<SpriteRenderer>().sprite = eyeList[1];
            eyes.GetComponent<SpriteRenderer>().flipY = true;
            eyes.localPosition = new Vector3(-0.2f, 0.25f, 0);
        } else {
            eyes.GetComponent<SpriteRenderer>().sprite = eyeList[0];
            eyes.localPosition = new Vector3(0, 0.25f, 0);
        }

        // Blink if in mercy invincibility
        if (MainCharacter.S.mercyInvincible) {
            sprend.enabled = !sprend.enabled;
        }
        else {
            sprend.enabled = true;
        }

        // Activate or deactivate mounting prompt
        if ((transform.position - MainCharacter.S.transform.position).magnitude < maxMountDist && !mounted
            && SceneManager.GetActiveScene() == SceneManager.GetSceneByName("_Scene_3")) {
            prompt.enabled = true;
        } else {
            prompt.enabled = false;
        }
    }

    void OnTriggerEnter(Collider coll) {
        if (MainCharacter.S.mercyInvincible) {
            return;
        }

        if (coll.gameObject.layer == LayerMask.NameToLayer("Enemy")) {
            MainCharacter.S.HitEnemy(coll);
        }
    }

    // Check which direction the player is aiming, and alter the direction the weapon is facing to match.
    void CheckDirection() {
        float horiz = 0f;
        float vert = 0f;
        if (device != null) {
            horiz = device.RightStickX.Value; //Input.GetAxis("King_HorizontalAim");
            vert = device.RightStickY.Value; //Input.GetAxis("King_VerticalAim");
        }
        
        if (horiz == 0) {
            if (vert > 0) {
                angle = 90;
            } else if (vert < 0) {
                angle = -90;
            } else {
                return;
            }
        } else {
            angle = Mathf.Atan(vert / horiz);

            if (horiz < 0) {
                angle += Mathf.PI;
            }

            angle *= 180 / Mathf.PI;
        }

        weapon.rotation = Quaternion.Euler(0, 0, angle);
    }

    // Check if the player should fire a shot, and fire one if so
    void FireControl() {
        if (lightningCharging) {
            if (Time.time > lightningChargeStart + lightningChargeTime) {
                FireLightning();
                lightningCharging = false;
            }
            return;
        }

        if (device != null && device.LeftTrigger.WasPressed && Time.time > timeBetweenLightning + lastLightningTime) {
            lightningChargeStart = Time.time;
            lightningCharging = true;
            return;
        }

        if (device != null && device.RightTrigger.Value > 0 
            && ((Time.time > timeBetweenShots_Mounted + lastShotTime && mounted) || (Time.time > timeBetweenShots_Dismounted + lastShotTime && !mounted))) {
            FireMissile();
        }
    }

    // Fire a missile in the direction the player is pointing
    void FireMissile() {
        // Add slight error to the missile direction
        float error = Random.Range(-10f, 10f);

        GameObject go = Instantiate(missile);
        go.transform.position = transform.position + Quaternion.Euler(0, 0, angle) * Vector3.right;
        go.GetComponent<Rigidbody>().velocity = Quaternion.Euler(0, 0, angle + error) * (Vector3.right * 30f);
        lastShotTime = Time.time;

        missileSound.Play();
    }

    // Fire a lightning bolt in the direction the player is pointing
    void FireLightning() {
        RaycastHit hitObj;
        bool hitSomething = Physics.Raycast(transform.position, weapon.transform.rotation * Vector3.right, out hitObj, 100f, groundPhysicsLayerMask);

        float dist;
        if (hitSomething) {
            dist = hitObj.distance;
        } else {
            dist = 100f;
        }

        GameObject go = Instantiate(bolt);
        go.transform.rotation = weapon.rotation * Quaternion.Euler(0, 0, 90);
        Vector3 scale = go.transform.localScale;
        scale.y = dist * 0.5f;
        go.transform.localScale = scale;
        go.transform.position = weapon.transform.rotation * (Vector3.right * dist * 0.5f) + transform.position;
        lastLightningTime = Time.time;
        lightningCharging = false;

        lightningSound.Play();
    }

    // Get the king's movement inputs
    void GetMovement() {
        if (device == null) { return; }
        currentHorizSpeed = device.LeftStickX.Value * maxHorizSpeed;

        if (lightningCharging) {
            currentHorizSpeed = 0;
        }

        if (currentHorizSpeed < 0 && leftBounded) {
            currentHorizSpeed = 0;
        }

        if (currentHorizSpeed > 0 && rightBounded) {
            currentHorizSpeed = 0;
        }

        checkGrounded();

        if (device.Action1.WasPressed && grounded) {
            rigid.velocity += new Vector3(0, jumpSpeed, 0);
            jumpSound.Play();
        }
    }

    // Check if the player is on the ground or not
    void checkGrounded() {
        grounded = Physics.Raycast(transform.position, Vector3.down, 0.6f, groundPhysicsLayerMask);
        if (grounded) {
            usingGravity = false;
        } else {
            usingGravity = true;
        }

        leftBounded = Physics.Raycast(transform.position, Vector3.left, 0.6f, groundPhysicsLayerMask);
        rightBounded = Physics.Raycast(transform.position, Vector3.right, 0.6f, groundPhysicsLayerMask);
    }

    // If the player presses the Mount button, mount up or dismount
    void ChangeMount() {
        if (mounted) {
            if (device.Action3.WasPressed || device.Action1.WasPressed) {
                Dismount();
            }
        } else if (device.Action3.WasPressed) {
            if ((transform.position - MainCharacter.S.transform.position).magnitude < maxMountDist) {
                MountUp();
            }
        }
    }

    // Update the king's position to right above the main character
    void UpdatePosition() {
        transform.position = MainCharacter.S.transform.position + Vector3.up * 1.5f;
    }

    // Move the king in accordance with his inputs
    void Move() {
        // Move the king horizontally
        if (rigid == null) { return; }
        Vector3 vel = rigid.velocity;
        vel.x = currentHorizSpeed;

        // Modify velocity with gravity
        if (usingGravity) {
            vel.y -= gravity * Time.fixedDeltaTime;
        }

        rigid.velocity = vel;
    }

    void Dismount() {
        mounted = false;
        usingGravity = true;
        rigid.velocity = new Vector3(0, jumpSpeed, 0);
        GetComponent<SphereCollider>().isTrigger = false;
        jumpSound.Play();
    }

    void MountUp() {
        mounted = true;
        usingGravity = false;
        GetComponent<SphereCollider>().isTrigger = true;
    }
}
