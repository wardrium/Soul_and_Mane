﻿using UnityEngine;
using System.Collections;

public class SceneChanger : MonoBehaviour {

    // Use this for initialization
    public NextLevelTrigger Liontrigger, Kingtrigger;
    public string NextLevelName;
    public bool goToNextLevel;
    bool transitionDone = true;
    FadeOut fade;
    public bool oneDoorSceneSwitch;

    public bool lockedByEnemies;
    public bool locked;

    void Start()
    {

        fade = FindObjectOfType<FadeOut>();
        oneDoorSceneSwitch = name == "OneDoorSceneSwitch";

        if (lockedByEnemies) {
            locked = true;
        } else {
            locked = false;
        }

    }

    void Update()
    {

        if (goToNextLevel && !locked)
        {

            if (fade != null && fade.sprend.color.a == 1)
            {

                Application.LoadLevel(NextLevelName);

            }

        }

        else if(oneDoorSceneSwitch)
        {

            Vector3 king = FindObjectOfType<King>().transform.position;
            Vector3 lion = FindObjectOfType<MainCharacter>().transform.position;

            /*print(king);
            print(lion);

            print(king.x <= transform.position.x + 2 && king.x >= transform.position.x - 2 &&
                king.y >= transform.position.y - 3 && king.y <= transform.position.y + 3);
            print(lion.x <= transform.position.x + 2 && lion.x >= transform.position.x - 2 &&
                lion.y >= transform.position.y - 3 && lion.y <= transform.position.y + 3);*/

            if (king.x <= transform.position.x + 2 && king.x >= transform.position.x - 2 &&
                king.y >= transform.position.y - 3 && king.y <= transform.position.y + 3 &&
                lion.x <= transform.position.x + 2 && lion.x >= transform.position.x - 2 &&
                lion.y >= transform.position.y - 3 && lion.y <= transform.position.y + 3)
            {
                if (!locked) {
                    goToNextLevel = true;
                }

            }

        }

    }

    void FixedUpdate()
    {
        // If there are any enemies, don't unlock the door
        if (lockedByEnemies) {
            GameObject[] go = GameObject.FindGameObjectsWithTag("Narwhal");
            GameObject[] go2 = GameObject.FindGameObjectsWithTag("Elephant");
            GameObject[] go3 = GameObject.FindGameObjectsWithTag("Hopper");

            if (go.Length == 0 && go2.Length == 0 && go3.Length == 0) {
                locked = false;
            }
        }

        if (locked) {
            Liontrigger.canSwitchLevels = false;
            Kingtrigger.canSwitchLevels = false;
        }

        if (Liontrigger.canSwitchLevels && Kingtrigger.canSwitchLevels && !locked)
        {
            

            goToNextLevel = true;
            if (fade != null)
                fade.getDarker();

        }

    }

}
