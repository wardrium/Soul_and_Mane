﻿using UnityEngine;
using System.Collections;

public class Elephant : Enemy {
    public Sprite[] sprites;
    private int spriteIndex;

    public GameObject bomb_prefab;

    // Modify these for elephant behavior
    private float maxHealth = 20f;
    public float attack_animation_cooldown = 0.1f; // How long to wait before updating animation.
    public float attack_cooldown = 2.0f;
    public float bomb_min_y_velocity = 2.0f;
    public float bomb_max_y_velocity = 10.0f;

    private bool attacking = false;
    private float time_since_last_animation_change = 0.0f;
    private float time_since_last_attack = 0.0f;

	void Start () {
        facing_left = true;
        health = maxHealth;
        spriteIndex = 0;
        sprend = this.GetComponent<SpriteRenderer>();
	}
	
	public override void Update () {
        Vector3 closer;
        if ((King.S.transform.position - transform.position).magnitude > (MainCharacter.S.transform.position - transform.position).magnitude) {
            closer = MainCharacter.S.transform.position;
        }
        else {
            closer = King.S.transform.position;
        }

        if (closer.x > transform.position.x) {
            facing_left = false;
            transform.rotation = Quaternion.Euler(0, 180, 0);
        } else {
            facing_left = true;
            transform.rotation = Quaternion.identity;
        }

        if (attacking) {
            time_since_last_animation_change += Time.deltaTime;
            if (time_since_last_animation_change >= attack_animation_cooldown) {
                time_since_last_animation_change = 0.0f;
                spriteIndex++;
                if (spriteIndex == sprites.Length) {
                    attacking = false;
                    spriteIndex = 0;
                    ShootBomb();
                }
                sprend.sprite = sprites[spriteIndex];
            }
        }
        else {
            time_since_last_attack += Time.deltaTime;
            if (time_since_last_attack >= attack_cooldown) {
                time_since_last_attack = 0.0f;
                attacking = true;
            }
        }
	}

    public override void HitMissile() {
        base.HitMissile();
        health -= 2;
        if (health <= 0) {
            Die();
        }
    }

    public override void HitBolt() {
        base.HitBolt();
        health -= 5;
        if (health <= 0) {
            Die();
        }
    }

    private void ShootBomb() {
        float x_mod = 0.0f;
        if (facing_left) {
            x_mod = -0.9f;
        }
        else {
            x_mod = 0.9f;
        }
        GameObject bomb = GameObject.Instantiate(bomb_prefab, new Vector3(this.transform.position.x + x_mod, this.transform.position.y + 1.0f, this.transform.position.z), Quaternion.identity) as GameObject;
        bomb.GetComponent<Rigidbody>().velocity = new Vector3(10.0f * x_mod, Random.Range(bomb_min_y_velocity, bomb_max_y_velocity), 0.0f);
    }

    public override void Die() {
        base.Die();

        Destroy(gameObject);
    }
}
