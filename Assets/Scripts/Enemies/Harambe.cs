﻿using UnityEngine;
using System.Collections;

public class Harambe : Enemy {
    public Sprite[] sprites;
    public GameObject bomb_prefab;
    public GameObject ball_prefab;
    private int spriteIndex = 0;

    private int state = 0;  // 0 = idle, 1 = jumping, 2 = attacking
    public float ball_attack_interval = 5.0f;
    public float ball_distance = 7.0f;
    public float ball_speed = 15.0f;
    public float bomb_attack_interval = 10.0f;
    float ball_attack_time = 0.0f;
    float bomb_attack_time = 0.0f;
    public int num_bombs = 10;
    public float attack_animation_time = 1.0f;
    float animation_attack_time = 0.0f;
    public float jump_time = 2.0f;
    float current_jump_time = 0.0f;
    public float animation_change_time = 1.0f;
    float current_animation_time = 0.0f;

    public float time_until_turn = 1.0f;
    float time_since_turn = 0.0f;
    float x_velo;

    void Start() {
        sprend = transform.GetComponent<SpriteRenderer>();
        health = 100.0f;
    }

	void Update () {
        if (state == 0) {
            current_animation_time += Time.deltaTime;
            if (current_animation_time >= animation_change_time) {
                spriteIndex += 1;
                if (spriteIndex == 2) {
                    spriteIndex = 0;
                }
                this.GetComponent<SpriteRenderer>().sprite = sprites[spriteIndex];
                current_animation_time = 0.0f;
            }
        }

        ball_attack_time += Time.deltaTime;
        if (ball_attack_time >= ball_attack_interval) {
            state = 1;
            Jump();
            Invoke("BallAttack", 1.5f);
            ball_attack_time = 0.0f;
        }
        bomb_attack_time += Time.deltaTime;
        if (bomb_attack_time >= bomb_attack_interval) {
            current_animation_time = 0.0f;
            state = 2;
            this.GetComponent<SpriteRenderer>().sprite = sprites[2];
            BombAttack();
            bomb_attack_time = 0.0f;
        }

        if (state == 1) {
            current_jump_time += Time.deltaTime;
            if (current_jump_time >= jump_time) {
                state = 0;
                current_jump_time = 0.0f;
            }
        }
        if (state == 2) {
            animation_attack_time += Time.deltaTime;
            if (animation_attack_time >= attack_animation_time) {
                state = 0;
                animation_attack_time = 0.0f;
            }
        }

        time_since_turn += Time.deltaTime;
        if (time_since_turn >= time_until_turn) {
            time_since_turn = 0.0f;
            x_velo = Random.Range(5f, 10f);
            if (Random.Range(0, 2) < 1) {
                x_velo *= -1;
            }
        }
        this.transform.position = new Vector3(this.transform.position.x + x_velo * Time.deltaTime, this.transform.position.y, this.transform.position.z);
	}

    void Jump() {
        this.GetComponent<SpriteRenderer>().sprite = sprites[1];
        this.GetComponent<Rigidbody>().velocity = new Vector3(0.0f, 15.0f, 0.0f);
    }

    void BallAttack() {
        float ball_distance_side = Mathf.Sqrt(2 * (ball_distance * ball_distance)) * 0.5f;
        float ball_speed_side = Mathf.Sqrt(2 * (ball_speed * ball_speed)) * 0.5f;

        GameObject ball = GameObject.Instantiate(ball_prefab, new Vector3(this.transform.position.x - ball_distance, this.transform.position.y, this.transform.position.z), Quaternion.identity) as GameObject;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(-ball_speed, 0.0f, 0.0f);

        ball = GameObject.Instantiate(ball_prefab, new Vector3(this.transform.position.x - ball_distance_side, this.transform.position.y + ball_distance_side, this.transform.position.z), Quaternion.identity) as GameObject;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(-ball_speed_side, ball_speed_side, 0.0f);

        ball = GameObject.Instantiate(ball_prefab, new Vector3(this.transform.position.x, this.transform.position.y + ball_distance, this.transform.position.z), Quaternion.identity) as GameObject;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(0.0f, ball_speed, 0.0f);
        
        ball = GameObject.Instantiate(ball_prefab, new Vector3(this.transform.position.x + ball_distance_side, this.transform.position.y + ball_distance_side, this.transform.position.z), Quaternion.identity) as GameObject;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(ball_speed_side, ball_speed_side, 0.0f);

        ball = GameObject.Instantiate(ball_prefab, new Vector3(this.transform.position.x + ball_distance, this.transform.position.y, this.transform.position.z), Quaternion.identity) as GameObject;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(ball_speed, 0.0f, 0.0f);

        ball = GameObject.Instantiate(ball_prefab, new Vector3(this.transform.position.x + ball_distance_side, this.transform.position.y - ball_distance_side, this.transform.position.z), Quaternion.identity) as GameObject;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(ball_speed_side, -ball_speed_side, 0.0f);

        ball = GameObject.Instantiate(ball_prefab, new Vector3(this.transform.position.x, this.transform.position.y - ball_distance, this.transform.position.z), Quaternion.identity) as GameObject;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(0.0f, -ball_speed_side, 0.0f);
        
        ball = GameObject.Instantiate(ball_prefab, new Vector3(this.transform.position.x - ball_distance_side, this.transform.position.y - ball_distance_side, this.transform.position.z), Quaternion.identity) as GameObject;
        ball.GetComponent<Rigidbody>().velocity = new Vector3(-ball_speed_side, -ball_speed_side, 0.0f);
    }

    void BombAttack() {
        for (int i = 0; i < num_bombs; ++i) {
            float x_pos = Random.Range(-ball_distance, ball_distance);
            float y_pos = Random.Range(-ball_distance, ball_distance);
            GameObject bomb = GameObject.Instantiate(bomb_prefab, new Vector3(this.transform.position.x + x_pos, this.transform.position.y + y_pos, this.transform.position.z), Quaternion.identity) as GameObject;
            float x_vel = Random.Range(10.0f, 20.0f);
            float y_vel = Random.Range(10.0f, 20.0f);
            float x_mult = 1.0f;
            float y_mult = 1.0f;
            if (x_pos < 0) {
                x_mult = -1.0f;
            }
            if (y_pos < 0) {
                y_mult = -1.0f;
            }
            bomb.GetComponent<Rigidbody>().velocity = new Vector3(x_vel * x_mult, y_vel * y_mult);
        } 
    }

    public override void HitMissile() {
        base.HitMissile();
        health -= 2;
        if (health <= 0) {
            Die();
        }
    }

    public override void HitBolt() {
        base.HitBolt();
        health -= 5;
        if (health <= 0) {
            Die();
        }
    }

    public override void Die() {
        base.Die();
        Destroy(gameObject);
    }
}
