﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    public SpriteRenderer sprend;

    public bool facing_left = true;

    public float swipeTime = 0;
    private float maxSwipeTime = .5f;
    private GameObject lastProjectileHit = null;

    public float health;

    void Start() {
        sprend = this.GetComponent<SpriteRenderer>();
    }

    public virtual void Initialize(bool facing_left0){
        // Note: facing_left should be set in Start function of the enemy subclass script to the default direction.
        SetDirection(facing_left0);
        
    }
	
	// Update is called once per frame
	public virtual void Update () {

        if (health <= 0) {
            Die();
        }

        if(swipeTime < maxSwipeTime)
        {

            Rigidbody r = GetComponent<Rigidbody>();

            swipeTime += Time.deltaTime;

            if(swipeTime >= maxSwipeTime)
            {

                if(r != null)
                {

                    r.velocity = new Vector3(0, r.velocity.y, 0);

                }

            }

        }

	}

    public void SwipedFunction(Vector3 v)
    {

        Rigidbody r = GetComponent<Rigidbody>();
        if(r != null)
        {

            if (swipeTime < maxSwipeTime)
            {

                if (v.x <= transform.position.x)
                {

                    r.velocity = new Vector3(6, r.velocity.y, 0);

                }

                else
                {
                    r.velocity = new Vector3(-6, r.velocity.y, 0);

                }

            }

        }

    }

    // Handle impacts with player projectiles
    void OnTriggerEnter(Collider coll) {
        if (coll.CompareTag("KingMissile")) {
            if (coll.gameObject != lastProjectileHit) {
                HitMissile();
                Destroy(coll.gameObject);
            }
        } else if (coll.CompareTag("KingBolt")) {
            HitBolt();
        }

        else if(coll.name == "Swipe(Clone)")
        { 
           
        }
    }

    public void SetDirection(bool facing_left0) {
        if (facing_left != facing_left0) {
            sprend.flipX = true;
            facing_left = facing_left0;
        }
    }

    public virtual void Move() { }

    public virtual void HitMissile() {}

    public virtual void HitBolt() { }

    public virtual void Die() {
    }
}
