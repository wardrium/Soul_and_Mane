﻿using UnityEngine;
using System.Collections;

public class TestEnemy : Enemy {

    private float moveSpeed = 5f;

    private float freezeTime;
    private float missileFreezeTime = 0.3f;
    private float boltFreezeTime = 0.8f;
    private float startFreeze;
    private bool frozen;

	// Use this for initialization
	void Start () {
        health = 10;
	}
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
	    if (frozen && Time.time > startFreeze + freezeTime) {
            frozen = false;
        }
	}

    void FixedUpdate () {
        Move();
    }

    // Move towards the king's position
    public override void Move() {
        base.Move();

        if (frozen) {
            return;
        }

        Vector3 moveOffset = King.S.transform.position - transform.position;

        if (moveOffset.magnitude >= moveSpeed * Time.fixedDeltaTime) {
            moveOffset = moveOffset.normalized * moveSpeed * Time.fixedDeltaTime;

            transform.position += moveOffset;
        } else {
            transform.position = King.S.transform.position;
        }
    }

    public override void HitMissile() {
        base.HitMissile();

        Freeze(missileFreezeTime);
        health -= 1;
    }

    public override void HitBolt() {
        base.HitBolt();

        Freeze(boltFreezeTime);
        health -= 5;
    }

    public override void Die() {
        base.Die();

        transform.position = Vector3.zero;
        health = 10;
    }

    void Freeze(float time) {
        if (!frozen || time > freezeTime - (Time.time - startFreeze)) {
            freezeTime = time;
            frozen = true;
            startFreeze = Time.time;
        }
    }
}
