﻿using UnityEngine;
using System.Collections;

public class Narwhal : Enemy {

    public Sprite[] sprites;

    private float flapSpeed = 2;
    private Transform fwing, bwing;

    private float maxHealth = 10;

    private int state = 0;  // 0 = Waiting, 1 = Getting ready to charge, 2 = Charging, 3 = Resting
    public float charge_time = 1.0f;
    public float charge_speed = 10.0f;
    private float current_charge_time = 0.0f;
    private float target_charge_time = 0.0f;
    private Vector3 charge_target;   // Either king or lion's position
    public float aggro_distance = 15.0f;
    private float startedPhase;
    public float rest_time = 1.0f;      // How long narwhal should wait before aggroing again.
    private float current_rest_time = 0.0f;
    private Vector3 origin;
    private Vector3 dest;

	// Use this for initialization
	void Start () {
        sprend = transform.Find("Body").GetComponent<SpriteRenderer>();
        health = maxHealth;
        startedPhase = Time.time;
        fwing = transform.Find("WingF");
        bwing = transform.Find("WingB");
	}
	
	// Update is called once per frame
	public override void Update () {
        Move();
	}

    void LateUpdate() {
        float interval = maxHealth / sprites.Length;

        for (int i = 0; i < sprites.Length; ++i) {
            if (health >= i * interval) {
                sprend.sprite = sprites[i];
            }
        }

        if (dest.x > origin.x) {
            transform.rotation = Quaternion.Euler(0, 180, 90);
        } else {
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }

        float yscale = Mathf.Sin(Time.time * flapSpeed);
        fwing.localScale = new Vector3(1, yscale, 1);
        bwing.localScale = new Vector3(1, yscale, 1);
    }

    public override void HitMissile() {
        base.HitMissile();
        health -= 2;
        if (health <= 0) {
            Die();
        }
    }

    public override void HitBolt() {
        base.HitBolt();
        health -= 5;
        if (health <= 0) {
            Die();
        }
    }

    public override void Move() {
        if (state == 0) {
            float king_dist = Vector3.Distance(this.transform.position, King.S.transform.position);
            float lion_dist = Vector3.Distance(this.transform.position, MainCharacter.S.transform.position);
            if (king_dist < aggro_distance || lion_dist < aggro_distance){
                if (king_dist < lion_dist)
                    charge_target = King.S.transform.position;
                else
                    charge_target = MainCharacter.S.transform.position;
                state = 1;
                current_charge_time = 0f;
                sprend.color = new Color(0.8f, 0.8f, 0f);
            }
        }
        else if (state == 1) {
            current_charge_time += Time.deltaTime;
            if (current_charge_time >= charge_time) {
                state = 2;
                origin = this.transform.position;
                float x_mod = Random.Range(-2.0f, 2.0f);
                float y_mod = Random.Range(-2.0f, 2.0f);
                dest = new Vector3(charge_target.x + x_mod, charge_target.y + y_mod, charge_target.z);
                current_charge_time = 0.0f;
                target_charge_time = Vector3.Distance(origin, dest) / charge_speed;
                sprend.color = new Color(1, 1, 1);
            }
        }
        else if (state == 2) {
            current_charge_time += Time.deltaTime;
            if (current_charge_time >= target_charge_time) {
                this.transform.position = dest;
                state = 3;
                current_rest_time = 0.0f;
            }
            else {
                this.transform.position = Vector3.Lerp(origin, dest, current_charge_time / target_charge_time);
            }
        }
        else if (state == 3) {
            current_rest_time += Time.deltaTime;
            if (current_rest_time >= rest_time) {
                state = 0;
            }
        }
    }

    public override void Die() {
        base.Die();
        Destroy(gameObject);
    }

    public void Swiped() {
        startedPhase = Time.time;
    }
}
