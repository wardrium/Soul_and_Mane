﻿using UnityEngine;
using System.Collections;

public class Worm : Enemy {

    public Sprite[] sprites;
    public GameObject[] pass_through;   // Gameobjects this worm should pass through

    public int direction = 0;   // 0 = Up, 1 = Right, 2 = Down, 3 = Left
    public float trigger_distance = 5.0f;
    public float animation_change_time = 0.3f;  // How often to change animations
    public float move_speed = 5.0f;

    float current_animation_time = 0.0f;
    int sprite_index;
    bool waiting = true;

	void Start () {
        sprend = this.GetComponent<SpriteRenderer>();
        for (int i = 0; i < pass_through.Length; ++i) {
            Physics.IgnoreCollision(this.GetComponent<Collider>(), pass_through[i].GetComponent<Collider>());
        }
	}
	
	void Update () {
        if (waiting) {
            if (Vector3.Distance(this.transform.position, King.S.transform.position) <= trigger_distance ||
                Vector3.Distance(this.transform.position, MainCharacter.S.transform.position) <= trigger_distance) {
                    Awaken();
            }
        }
        else {
            Move();
            current_animation_time += Time.deltaTime;
            if (current_animation_time >= animation_change_time) {
                current_animation_time = 0.0f;
                sprite_index += 1;
                if (sprite_index >= sprites.Length) {
                    sprite_index = 0;
                }
                sprend.sprite = sprites[sprite_index];
            }
        }
	}

    void Awaken() {
        waiting = false;
        sprend.sprite = sprites[0];
        this.GetComponent<BoxCollider>().size = new Vector3(1.4f, 17f, 0.0f);
        if (direction == 0) {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 7.5f, this.transform.position.z);
        }
        else if (direction == 1) {
            this.transform.position = new Vector3(this.transform.position.x + 7.5f, this.transform.position.y, this.transform.position.z);
        }
        else if (direction == 2) {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 7.5f, this.transform.position.z);
        }
        else if (direction == 3) {
            this.transform.position = new Vector3(this.transform.position.x - 7.5f, this.transform.position.y, this.transform.position.z);
        }
        
        sprite_index = 0;
    }

    public override void Move() {
        float pos_change = Time.deltaTime * move_speed;
        if (direction == 0) {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + pos_change, this.transform.position.z);
        }
        else if (direction == 1){
            this.transform.position = new Vector3(this.transform.position.x + pos_change, this.transform.position.y, this.transform.position.z);
        }
        else if (direction == 2) {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - pos_change, this.transform.position.z);
        }
        else if (direction == 3) {
            this.transform.position = new Vector3(this.transform.position.x - pos_change, this.transform.position.y, this.transform.position.z);
        }
    }

    void OnCollisionEnter(Collision coll) {
        if (coll.gameObject.tag == "Weak Solid") {
            Destroy(coll.gameObject);
        }
    }

    void OnTriggerEnter(Collider coll) {
        if (coll.gameObject.tag == "Door") {
            Destroy(coll.gameObject);
        }
    }

}
