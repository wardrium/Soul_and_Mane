﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class enemyController : MonoBehaviour {

    public enemySpawner[] spawners;
    public string nextScene;
	
	// Update is called once per frame
	void Update () {
        bool finished = true;
        for (int i = 0;  i < spawners.Length; ++i) {
            if (spawners[i].numEnemies > 0) {
                finished = false;
            }
        }

        if (finished) {
            SceneManager.LoadScene(nextScene);
        }
	}
}
