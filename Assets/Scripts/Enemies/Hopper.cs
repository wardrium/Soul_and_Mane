﻿using UnityEngine;
using System.Collections;

public class Hopper : Enemy {

    private Rigidbody rigid;

    public Sprite neutral, up, down;

    public float freezeTime;
    public float missileFreezeTime = 0.3f;
    public float boltFreezeTime = 0.8f;
    public float startFreeze;
    public bool frozen;

    private float startPrepTime;
    private float maxPrepTime = 3;

    public float gravity = 10f;
    public bool grounded;

    public Vector3 hopVector = new Vector3(5f, 5f, 0);

    // Use this for initialization
    void Start () {
        health = 15;

        rigid = GetComponent<Rigidbody>();

        sprend = transform.Find("Sprite").GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
        if (frozen && Time.time > freezeTime + startFreeze) {
            frozen = false;
        }

        if (grounded && Time.time > startPrepTime + maxPrepTime) {
            Hop();
        }

        if((rigid.velocity.x < 0 && !canMoveLeft()) ||
           rigid.velocity.x > 0 && !canMoveRight())
        {

            rigid.velocity = new Vector3(0, rigid.velocity.y, 0);

        }

    }

    bool canMoveRight()
    {

        return !Physics.Raycast(transform.position, new Vector3(1, 0, 0), 2.5f, LayerMask.GetMask("Environment"));

    }

    bool canMoveLeft()
    {

        return !Physics.Raycast(transform.position, new Vector3(-1, 0, 0), 2.5f, LayerMask.GetMask("Environment"));

    }

    void LateUpdate() {
        if (rigid.velocity.y > 0) {
            sprend.sprite = up;
        } else if (rigid.velocity.y < 0) {
            sprend.sprite = down;
        } else {
            sprend.sprite = neutral;
        }
    }

    void FixedUpdate() {
        if (Physics.Raycast(transform.position, Vector3.down, 0.6f, LayerMask.GetMask("Environment"))) {
            if(!grounded)
            {

                FindObjectOfType<CameraFollowScript>().startJitter(.50f, .5f);
                GetComponent<AudioSource>().Play();

            }
            grounded = true;
        } else {
            grounded = false;
        }

        if (grounded && rigid.velocity.y < 0) {
            startPrepTime = Time.time;
            rigid.velocity = Vector3.zero;
        }

        if (!grounded) {
            rigid.velocity += new Vector3(0, -gravity * Time.fixedDeltaTime, 0);
        }
    }

    void Hop() {
        Vector3 trueVec = hopVector;

        Vector3 dest;
        
        if ((King.S.transform.position - transform.position).magnitude > (MainCharacter.S.transform.position - transform.position).magnitude) {
            dest = MainCharacter.S.transform.position;
        } else {
            dest = King.S.transform.position;
        }

        if (dest.x < transform.position.x) {
            trueVec.x *= -1;
        }

        rigid.velocity = trueVec;
    }

    public override void HitMissile() {
        base.HitMissile();

        Freeze(missileFreezeTime);
        health -= 2;
        if (health <= 0) {
            Die();
        }
    }

    public override void HitBolt() {
        base.HitBolt();

        Freeze(boltFreezeTime);
        health -= 10;
        if (health <= 0) {
            Die();
        }
    }

    public override void Die() {
        base.Die();

        Destroy(gameObject);
    }

    void Freeze(float time) {
        if (!frozen || time > freezeTime - (Time.time - startFreeze)) {
            freezeTime = time;
            frozen = true;
            startFreeze = Time.time;
        }
    }
}
