﻿using UnityEngine;
using System.Collections;

public class enemySpawner : MonoBehaviour {

    public GameObject enemy;

    public int numEnemies = 20;

    public float spawnRate = 3;

    private float spawnIncrease = .95f;

    private float lastSpawnTime;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (numEnemies == 0) {
            return;
        }

	    if (Time.time > lastSpawnTime + spawnRate) {
            GameObject go = Instantiate(enemy);

            go.transform.gameObject.transform.gameObject.transform.position = transform.position;

            spawnRate *= spawnIncrease;

            numEnemies -= 1;

            lastSpawnTime = Time.time;
        }
	}
}
