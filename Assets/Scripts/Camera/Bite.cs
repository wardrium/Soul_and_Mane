﻿using UnityEngine;
using System.Collections;

public class Bite : MonoBehaviour {

    public GameObject bottomBite;
    public GameObject topBite;

    bool defaultPositionInitialized = false;
    public Vector3 defaultPosition;

    float life;

	// Use this for initialization
	void Start () {

        life = 0;
	
	}
	
	// Update is called once per frame
	void Update () {

        life += Time.deltaTime;

        if (life > .05f)
        {

            Destroy(gameObject);

        }

        Vector3 moveVec = new Vector3(0, .08f, 0);

        if(bottomBite.transform.position.y >= topBite.transform.position.y)
        {

            if(!defaultPositionInitialized)
            {

                defaultPositionInitialized = true;
                defaultPosition = bottomBite.transform.position;

            }

            bottomBite.transform.position = new Vector3(defaultPosition.x + Random.Range(0, .1f), defaultPosition.y + Random.Range(0, .1f), 0);
            topBite.transform.position = new Vector3(defaultPosition.x + Random.Range(0, .1f), defaultPosition.y + Random.Range(0, .1f), 0);
            return;

        }

        bottomBite.transform.position += moveVec;
        topBite.transform.position -= moveVec;



	}

}
