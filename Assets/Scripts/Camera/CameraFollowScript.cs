﻿using UnityEngine;
using System.Collections;

public class CameraFollowScript : MonoBehaviour
{

    King kref;
    MainCharacter lionRef;
    Camera cam;

    float maxZoomOut = 20;

    public bool isJittering = false;
    public float jitterCounter = 0, jitterDuration, jitterIntensity;
    Vector3 jitterFocus;

    GameObject u, r, l, d;

    //keep intensity between (0, 2];
    public void startJitter(float duration, float intensity)
    {

        jitterDuration = duration;
        jitterIntensity = intensity;

        if (!isJittering)
        {

            jitterFocus = new Vector3(transform.position.x, transform.position.y, -10);

        }

        isJittering = true;

    }

    void jitter()
    {

        jitterCounter += Time.deltaTime;

        transform.position = new Vector3(
            Random.Range(-jitterIntensity, jitterIntensity) * (1 - jitterCounter / jitterDuration) + transform.position.x,
            Random.Range(-jitterIntensity, jitterIntensity) * (1 - jitterCounter / jitterDuration) + transform.position.y,
            -10);

        if (jitterCounter >= jitterDuration)
        {

            isJittering = false;
            jitterCounter = 0;

        }

    }

    // Use this for initialization
    void Start()
    {

        kref = FindObjectOfType<King>();
        lionRef = FindObjectOfType<MainCharacter>();
        cam = GetComponent<Camera>();

        createNewMapConstraints();

        cam.orthographicSize = 15;

    }

    void createNewMapConstraints()
    {


        u = GameObject.CreatePrimitive(PrimitiveType.Cube);
        u.layer = 14;
        d = GameObject.CreatePrimitive(PrimitiveType.Cube);
        d.layer = 14;
        r = GameObject.CreatePrimitive(PrimitiveType.Cube);
        r.layer = 14;
        l = GameObject.CreatePrimitive(PrimitiveType.Cube);
        l.layer = 14;

        r.transform.localScale = new Vector3(1, 32, 1);
        l.transform.localScale = new Vector3(1, 32, 1);
        d.transform.localScale = new Vector3(40, 1, 1);
        u.transform.localScale = new Vector3(40, 1, 1);

        r.name = "CameraBounds(do not touch)";
        u.name = "CameraBounds(do not touch)";
        d.name = "CameraBounds(do not touch)";
        l.name = "CameraBounds(do not touch)";


    }

    void handleBoxes()
    {

        u.transform.position = new Vector3(transform.position.x, transform.position.y + 15.5f, 0);
        d.transform.position = new Vector3(transform.position.x, transform.position.y - 15.5f, 0);
        l.transform.position = new Vector3(transform.position.x + 20.5f, transform.position.y, 0);
        r.transform.position = new Vector3(transform.position.x - 20.5f, transform.position.y, 0);

    }

    // Update is called once per frame
    void Update()
    {

        ManageCamera();

        if (isJittering)
        {
            jitter();
        }

        handleBoxes();

    }

    //get average distance between two people
    void ManageCamera()
    {

        Vector3 middleGround = (kref.transform.position + lionRef.transform.position) / 2;
        Vector3 dest = new Vector3(middleGround.x, middleGround.y, -10);

        transform.position = Vector3.Lerp(transform.position, dest, 0.1f);

        Vector3 distance = kref.transform.position - lionRef.transform.position;

        float x = kref.transform.position.x - lionRef.transform.position.x;
        x *= x;
        float y = kref.transform.position.y - lionRef.transform.position.y;
        y *= y;

        float playerDistance = Mathf.Sqrt(x + y);


    }
}
