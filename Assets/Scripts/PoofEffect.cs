﻿using UnityEngine;
using System.Collections;

public class PoofEffect : MonoBehaviour {
    public Sprite[] poofSprite;

    public float timeToLive;
    public float timeStart;
    public float aniLength = 1f;

    public SpriteRenderer sprend;

	// Use this for initialization
	void Start () {
        timeStart = Time.time;
        timeToLive = Time.time + aniLength;

        sprend = this.gameObject.GetComponent<SpriteRenderer>();
        if (sprend == null) {
            sprend = this.gameObject.AddComponent<SpriteRenderer>();
            sprend.sortingOrder = 3;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (timeToLive < Time.time) {
            int phase = (int)Mathf.FloorToInt( 5 * (Time.time - timeStart)/aniLength );
            phase = Mathf.Min(phase, 4);

            sprend.sprite = poofSprite[phase];


        } else {
            Destroy(this.gameObject);
        }
	}
}
