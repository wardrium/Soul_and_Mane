  _________            .__      ____       _____                        
 /   _____/ ____  __ __|  |    /  _ \     /     \ _____    ____   ____  
 \_____  \ /  _ \|  |  \  |    >  _ </\  /  \ /  \\__  \  /    \_/ __ \ 
 /        (  <_> )  |  /  |__ /  <_\ \/ /    Y    \/ __ \|   |  \  ___/ 
/_______  /\____/|____/|____/ \_____\ \ \____|__  (____  /___|  /\___  >
        \/                           \/         \/     \/     \/     \/ 
========================================================================
Wolverine Soft Game Jam (Fall 2016)
EECS 494: Computer Game Design & Development
Project 2
========================================================================

Developers:
Ward Chiang		(wardrium@umich.edu)
Michael Dixon	(mdx@umich.edu)
Michael Lee		(mdwlmdwl@umich.edu)
Joseph Sorenson	(jsoren@umich.edu)

========================================================================

Instructions:
Requires two USB Controllers (ideally Xbox 360/One).
One player controls the King and the other controls the Lion.

King Controls:
A - Jump
X - Mount/Dismount Lion
RT - Magic Missile Spell
LT - Magic Beam Spell
Control Stick - Move

Lion Controls:
A - Jump
B - Swipe Attack
Control Stick - Move

========================================================================

Credits:
Thanks to Branden Carlson for donating his voice for the unused fanfare sound track.
All other art and sound assets were created by the developers

========================================================================